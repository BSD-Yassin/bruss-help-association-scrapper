### Brusshelp association-scrapper

#### Work in progress : 


1 - Need to fix the scrapper to provide a csv
2 - I still need to create the maps and the interactive maps if I ever get the courage.
https://www.youtube.com/watch?v=5za5I3kUuOI&ab_channel=MikaelCodes



Small scrapper of the [Brusshelp](http://www.brusshelp.org/) website for non-lucrative purpose. I plan to make a few maps with [Openstreemap](https://www.openstreetmap.org/) to provide for any people in need. 

Basically, it scraps in three steps, it takes every category into memory, then it reads every service within the category and then it writes it in a json file with a few options. In that step, it also uses the Geocoding service to retrieve the coordinates of the address.

This makes use of :

            * BeautifulSoup/bs4
            * Requests
            * Json
            * Prettymaps
            * Geocoding API

As simple as is.


If you want to use it, you can create an secret.json file for the API key
Or set it on the function

The proper website for the Geocoding is this : [Geocoding API](https://geocodify.com/#features)

##### Notes

This README, and this project was partly written by Github Copilot and Tabnine.
