from turtle import title
import folium
import requests
import json

class my_folium():
    def __init__(self, address, position=None, title="Default", zoom_start="12"):
        self.address = address
        self.position = position
        self.title = title
        self.zoom_start = zoom_start
        self.API = "31660fb4bb237333ba3fb84a3a5512f082a269b8"
    
    def get_geoposition(self, address):
        response = requests.get(f"https://api.geocodify.com/v2/geocode?api_key={self.API}&q={address}")    
        if response.status_code == 200:
            geo_dict = json.loads(response.text)
            # print(geo_dict) # Print the full geolocation
            coordinates = geo_dict['response']['features'][0]['geometry']['coordinates'] # Return the coordinates
            y = coordinates[0]
            x = coordinates[1]
        else:
            print('An error occurred while attempting to retrieve data from the API.')
            return None