from ast import main
from textwrap import indent
from time import sleep
from bs4 import BeautifulSoup
import requests
import csv
import json

# The right url to get the data from the website
url = "http://www.brusshelp.org/index.php/fr/portail-pro/help-in-brussels/joomlannuaire/defaut/16-plan-en-ligne"
root_url = "http://www.brusshelp.org"

class Scrape():
    def __init__(self, root_url, url):
        self.url = url
        self.root_url = root_url
        self.data = dict()
        self.association_dict = dict()
    
    def write_data(self,data, data_type="JSON"):

        def write_data_json(self, data, path="."):
            with open(f'{path}/data.json', 'a+') as f:
                json_object = json.dumps(data, indent=4)
                f.write(json_object)
            f.close()
        
        def write_data_csv(self, data, path="."):
            with open(f'{path}/data.csv', 'a+') as f:
                filewriter = csv.writer(f, delimiter='\n', quoting=csv.QUOTE_MINIMAL)
                
                # for the service, return everything known about it and put it in the csv file
                for item in data.items():                    
                    filewriter.writerow(item)
            f.close()

        if data_type == "JSON":
            write_data_json
        elif data_type == "CSV":
            write_data_csv
        else:
            'Something went wrong.'

    def website_scrapper(self):
        # simple request and parsing through bs4
        site = requests.get(self.url)
        soup = BeautifulSoup(site.text, "lxml")

        # find all the categories in the website page
        lookup = soup.find("ul", {"class": "catlist"})

        # find all the links in the categories
        categories_byli = lookup.find_all('li')
        categories_dict = dict()

        # for each link, find the title without unwanted strings and the relative link to the category
        for i in categories_byli:
            categories_dict[(i.text).replace('\n', '').replace('"', '').replace("'", '')] = i.find('a').get('href')
        
        self.data.update({"Catégories":categories_dict})
        return self.data

    def categories_scrapper(self):
        categories = [i for i in self.data.get('Catégories')]

        for i in categories:
            end_url = self.data["Catégories"][i]
            main_url = self.root_url + end_url
            website_category = requests.get(main_url)
            soup = BeautifulSoup(website_category.text, "lxml")

            # find all the categories in the website page
            lookup = soup.find("div", {"id": "itemlist"})    

            # find all the links in the categories
            categories_byli = lookup.find_all('a')
            temp_list = list()

            # for each link, find the title without unwanted strings and the relative link to the category
            for j in categories_byli:
                temp_list.append(j.get('href'))

            # update the dict for this category
            self.data.update({i:temp_list})
        return self.data
    
    def per_service_scrapper(self):
        categories_assoc = tuple(self.data.keys())[1:-1]
        for links in categories_assoc:
            links_per_cat = self.data[links]

            for association_link in links_per_cat: 
                main_url = root_url + association_link
                
                website_service = requests.get(main_url)
                soup = BeautifulSoup(website_service.text, "lxml")
                # geocoder_instance = Geocoding()
                
                lookup = soup.find("div", {"class": "item"})
                
                span = lookup.find_all('span')
                div = lookup.find_all('div')
                p = lookup.find_all('p')
                
                association_dict = dict()

                for i in span:
                    # print(i.__dict__)
                    try :
                        association_dict[i.attrs.get('class')[0]] =  i.contents
                    except : 
                        pass

                for j in div:
                        
                    try :
                        association_dict[j.attrs.get('class')[0]] = j.contents
                    except: 
                        pass
                
                for k in p:
                    try : 
                        association_dict[k.attrs.get('class')[0]] = k.contents
                    except: 
                        pass
                
        self.association_dict = association_dict
        return self.association_dict

    def clean_soup(object_str):
        
        if object_str == str:
            soup = BeautifulSoup(object_str, "lxml")
            object_str = soup.text.replace('\n', '').replace('"', '').replace("'", '')
         
        elif object_str == list:
            for i in object_str:
                soup = BeautifulSoup(i, "lxml")
                object_str = soup.text.replace('\n', '').replace('"', '').replace("'", '')
                return object_str
            
        else:
            return object_str

    def clean_data():
        pass

brusshelp = Scrape(root_url,url)
brusshelp.website_scrapper()
brusshelp.categories_scrapper()
brusshelp.per_service_scrapper()
brusshelp.write_data(brusshelp.data, "JSON")
brusshelp.write_data(brusshelp.association_dict, "JSON")
brusshelp.write_data(brusshelp.data, "CSV")
brusshelp.write_data(brusshelp.association_dict, "CSV")
