from bs4 import BeautifulSoup
import requests
import csv

# The right url to get the data from the website
url = "http://www.brusshelp.org/index.php/fr/portail-pro/help-in-brussels/joomlannuaire/defaut/16-plan-en-ligne"
root_url = "http://www.brusshelp.org"

class Website():
    def __init__(self, url, root_url):
        self.url = url
        self.root_url = root_url
        self.categories_dict = {}
        self.service_dict = {}
        self.association_dict = {}
    
    def write_data_csv(self, data, path, filename=None ):
 
        if type(data) == dict and filename != None:
            with open(f'{path}/{filename}.csv', 'a+') as f:
                filewriter = csv.writer(f, delimiter='\n', quoting=csv.QUOTE_MINIMAL)
                    
                # for the service, return everything known about it and put it in the csv file
                for item in data.items():                    
                    filewriter.writerow(item)
            f.close()

        elif type(data) == dict:
            for item in data.items():
                with open(f'{path}/{item[0]}.csv', 'a+') as f:
                    filewriter = csv.writer(f, delimiter='\n', quoting=csv.QUOTE_MINIMAL)
                        
                    # for the service, return everything known about it and put it in the csv file                 
                    filewriter.writerow(item[1])
                f.close()
                
        else :
            print("The data is not a dict or a list/tuple")
        
    def scrape_categories(self):

        # simple request and parsing through bs4
        site = requests.get(self.url)
        soup = BeautifulSoup(site.text, "lxml")

        # find all the categories in the website page
        lookup = soup.find("ul", {"class": "catlist"})

        # find all the links in the categories
        categories_byli = lookup.find_all('li')
        categories_dict = dict()

        # for each link, find the title without unwanted strings and the relative link to the category
        for i in categories_byli:
            categories_dict[(i.text).replace('\n', '').replace('"', '').replace("'", '')] = i.find('a').get('href')
        
        self.categories_dict = categories_dict
        return self.categories_dict



    # Part 2 : Scrap the services in each category
    def scrape_services(self, print:bool = False, print_type = None):
    # main loop per category
        for i in self.categories_dict.items():
            main_url = self.root_url + i[1]
            website_category = requests.get(main_url)
            soup = BeautifulSoup(website_category.text, "lxml")

            # find all the categories in the website page
            lookup = soup.find("div", {"id": "itemlist"})    

            # find all the links in the categories
            categories_byli = lookup.find_all('a')
            temp_list = list()

            # for each link, find the title without unwanted strings and the relative link to the category
            for j in categories_byli:
                temp_list.append(j.get('href'))
            
            # update the dict for this category
            self.service_dict[i[0]] = temp_list

        return self.service_dict


    # Part 3 : Scrape the detail for each service
    def scrape_perservice(self):
            
            count = 0
            for service_dict_list in self.service_dict.items():
                
                for association in service_dict_list[1]:
                        if count < 1 :
                            count =+ 1
                         
                            url_service = root_url + association
                            website_service = requests.get(url_service)
                            soup = BeautifulSoup(website_service.text, "lxml")
                            # geocoder_instance = Geocoding()
                            
                            lookup = soup.find("div", {"class": "item"})
                            children = lookup.findChildren(recursive=True)
                            
                            span = lookup.find_all('span')
                            div = lookup.find_all('div')
                            p = lookup.find_all('p')
                            
                            association_dict = dict()

                            for i in span:
                                # print(i.__dict__)
                                try :
                                    association_dict[i.attrs.get('class')[0]] =  i.contents
                                except : 
                                    pass

                            for j in div:
                                    
                                try :
                                    association_dict[j.attrs.get('class')[0]] = j.contents
                                except: 
                                    pass
                            
                            for k in p:
                                try : 
                                    association_dict[k.attrs.get('class')[0]] = k.contents
                                except: 
                                    pass
                        
                        else: 
                            pass
                    
                    ### Réussi à faire la liste intelligente sans devoir l'écrire, maintenant je dois différencier les informations importantes des autres. 
                    ### Il faut une fonction de filtre.
    def clean_string_soup(object_str):

        if object_str == str:
            soup = BeautifulSoup(object_str, "lxml")
            object_str = soup.text.replace('\n', '').replace('"', '').replace("'", '')
         
        elif object_str == list:
            for i in object_str:
                soup = BeautifulSoup(i, "lxml")
                object_str = soup.text.replace('\n', '').replace('"', '').replace("'", '')
                return object_str
            
        else:
            return object_str

"""
                    with open(f'{service_dict_list[0]}.csv', 'a') as csv_file:
                        writer = csv.DictWriter(csv_file, fieldnames=['name', 'category', 'address', 'geolocation', 'tel', 'mobile', 'fax', 'website', 'facebook', 'email', 'description', 'openinghours'])

                        # for the service, return everything known about it and put it in the csv file
                        for dict_item in data_dict.items() :
                            dict_item = list(dict_item)
                            dict_item[1] = str(dict_item[1]).replace('\n', '').replace('"', '').replace("'", '').replace(",", '')
                            writer.writerow({dict_item[0]:dict_item[1]})

                print(f'{service_dict_list[0]} is finished. {len(service_dict_list[1])} services found and written in{service_dict_list[0]}.csv.') 
""" 

brusshelp = Website(url=url, root_url=root_url)
brusshelp.scrape_categories()
brusshelp.scrape_services()
brusshelp.scrape_perservice()
brusshelp.clean_data()
print(brusshelp.association_dict)