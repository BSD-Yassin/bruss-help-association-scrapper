import pandas as pd
import json

class Geocoding:
    """ 
    This class focuses on the geocoding side of the project. It uses the adresses from the website to get the coordinates for later purpose. 
    This uses the Geocoding API from geocodify.com to get the coordinates. It is very simple to use, it returns a JSON that can then be filtered and extract the proper values from.
    """
    
    def __init__(self): 

        self.API = json.loads(open('secrets.json').read())['API_key']
    
    # Function to convert the adresses into geolocation
    def geocoding(self, adresse):
        response = requests.get(f"https://api.geocodify.com/v2/geocode?api_key={self.API}&q={adresse}")    
        if response.status_code == 200:
            geo_dict = json.loads(response.text)
            # print(geo_dict) # Print the full geolocation
            return geo_dict['response']['features'][0]['geometry']['coordinates'] # Return the coordinates
        else:
            print('An error occurred while attempting to retrieve data from the API.')
            return None


    def find_infos(filename):
        """
        Find the geodata of a given file
        """
        df = pd.read_csv(filename)
        print(df.head(2))
    